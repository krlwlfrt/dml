# @krlwlfrt/dml

[![pipeline status](https://img.shields.io/gitlab/pipeline/krlwlfrt/dml.svg?style=flat-square)](https://gitlab.com/krlwlfrt/dml/commits/master)
[![coverage](https://img.shields.io/gitlab/coverage/krlwlfrt/dml/master?style=flat-square)](https://gitlab.com/krlwlfrt/dml/-/pipelines)
[![npm](https://img.shields.io/npm/v/@krlwlfrt/dml.svg?style=flat-square)](https://npmjs.com/package/@krlwlfrt/dml)
[![license)](https://img.shields.io/npm/l/@krlwlfrt/dml.svg?style=flat-square)](https://opensource.org/licenses/GPL-3.0)
[![documentation](https://img.shields.io/badge/documentation-online-blue.svg?style=flat-square)](https://krlwlfrt.gitlab.io/dml)

## Example usage

Suppose you have a modular application that you want to extend with plugins, then you can use this module to load modules dynamically.

```typescript
import { loadModules } from '@krlwlfrt/dml';
import { join } from 'node:path';

(async () => {
  // set up your application

  const pluginDirectory = join(__dirname, 'plugins');

  // load plugins
  const modules = await loadModules(pluginDirectory);

  console.log(modules);

  // your application logic here
})();
```

### Hooks

The main functions `loadModules` and `loadModule` have an optional argument called `hooks`.
You can pass two hooks that fire just before loading a module and afterwards.

```typescript
import { loadModules } from '@krlwlfrt/dml';
import { join } from 'node:path';

(async () => {
  // set up your application

  const pluginDirectory = join(__dirname, 'plugins');

  // load plugins
  const modules = await loadModules(pluginDirectory, false, {
    afterLoad: async (loadedModule) => {
      console.info(`Loaded plugin ${loadedModule.path} with size ${loadedModule.size}.`);
    },
    beforeLoad: async (pathToLoad) => {
      // do not load modules, that contain 'foo' in their path
      if (/foo/.test(pathToLoad)) {
        return false;
      }

      return true;
    },
  });

  console.log(modules);

  // your application logic here
})();

```

## Documentation

See [online documentation](https://krlwlfrt.gitlab.io/dml) for detailed API description.
