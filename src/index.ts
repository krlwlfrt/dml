/*
 * Copyright (C) 2021 Karl-Philipp Wulfert
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * unknown WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {existsSync} from 'node:fs';
import {readdir, stat} from 'node:fs/promises';
import {basename, dirname, join} from 'node:path';

/**
 * A module
 */
export interface Module {
  /**
   * Exports of the module
   */
  exports: unknown;
  /**
   * Path of the module
   */
  path: string;
  /**
   * Size of the module
   */
  size: number;
}

/**
 * Before load hook
 * @param path Path to be loaded
 * @returns Whether or not the path shall be loaded
 */
export type BeforeLoadHook = (path: string) => Promise<boolean>;

/**
 * After load hook
 * @param module Loaded module
 */
export type AfterLoadHook = (loadedModule: Module) => Promise<void>;

/**
 * Hooks
 */
export interface Hooks {
  /**
   * After load hook
   */
  afterLoad?: AfterLoadHook;
  /**
   * Before load hook
   *
   * If the before load hook returns false, the module will not be loaded
   */
  beforeLoad?: BeforeLoadHook;
}

/**
 * An error that is thrown, when a "beforeLoad" hook returns false
 * This is handled internally and just used to distinguish between this and other errors.
 */
export class ShouldNotBeLoadedError extends Error {
  /**
   * Create a new error
   * @param pathToLoad Path to load
   */
  constructor(pathToLoad: string) {
    super(`${pathToLoad} should not be loaded.`);
  }
}

/**
 * Check whether a file is a JavaScript file or not
 * @param fileName File to check
 * @returns Whether or not a file is a JavaScript file
 */
export function isJavaScriptFile(fileName: string): boolean {
  return /\.js?$/.test(fileName);
}

/**
 * Check whether a file is a TypeScript file or not
 * @param fileName File to check
 * @returns Whether or not a file is a TypeScript file
 */
export function isTypeScriptFile(fileName: string): boolean {
  return /\.ts$/.test(fileName);
}

/**
 * Check whether or not currently running in NodeJs
 * @returns Whether or not currently running in NodeJS
 */
export function isRunningInNodeJS(): boolean {
  return 'process' in globalThis &&
    typeof globalThis.process === 'object' &&
    globalThis.process !== null &&

    'release' in globalThis.process &&
    typeof globalThis.process.release === 'object' &&
    globalThis.process.release !== null &&

    'name' in globalThis.process.release &&
    globalThis.process.release.name === 'node';
}

/**
 * Check whether or not currently running in TS Node
 * @returns Whether or not currently running in TS Node
 */
export function isRunningInTSNode(): boolean {
  return 'process' in globalThis &&
    typeof globalThis.process === 'object' &&
    globalThis.process !== null &&
    Symbol.for('ts-node.register.instance') in globalThis.process;
}

/**
 * Check whether or not currently running in Deno
 * @returns Whether or not currently running in Deno
 */
export function isRunningInDeno(): boolean {
  return 'Deno' in globalThis &&
    typeof globalThis['Deno' as keyof typeof globalThis] === 'object';
}

/**
 * Check whether or not currently running in TSX runtime
 * @returns Whether or not currently running in TSX runtime
 */
export function isRunningInTsx(): boolean {
  return 'process' in globalThis &&
    typeof globalThis.process === 'object' &&
    globalThis.process !== null &&

    'env' in globalThis.process &&
    typeof globalThis.process.env === 'object' &&
    globalThis.process.env !== null &&

    (
      '_' in globalThis.process.env &&
      typeof globalThis.process.env._ === 'string' &&
      globalThis.process.env._.includes('.bin/tsx')
    ) || (
      'npm_lifecycle_script' in globalThis.process.env &&
      typeof globalThis.process.env.npm_lifecycle_script === 'string' &&
      globalThis.process.env.npm_lifecycle_script === 'tsx'
    );
}

/**
 * Check whether nor not currently running in Bun
 * @returns Whether nor not currently running in Bun runtime
 */
export function isRunningInBun(): boolean {
  return 'Bun' in globalThis;
}

/**
 * Check whether or not currently running in TypeScript runtime
 * @returns Whether or not currently running in TypeScript runtime
 */
export function isRunningInTypeScriptRuntime(): boolean {
  return isRunningInDeno() ||
    isRunningInTSNode() ||
    isRunningInTsx() ||
    isRunningInBun() ||
    (
      isRunningInNodeJS() &&
      globalThis.process.execArgv.includes('--experimental-strip-types')
    );
}

/**
 * Load a module
 * @param pathToLoad File name of the module to load
 * @param hooks Hooks for loading modules
 * @returns A promise that resolves with the Module
 */
export async function loadModule(
  pathToLoad: string,
  hooks?: Hooks,
): Promise<Module> {
  try {
    // check if path to load exists
    const stats = await stat(pathToLoad);

    // check if path to load is a file
    if (!stats.isFile()) {
      throw new Error(`'${pathToLoad}' can not be loaded as a module because it is a directory.`);
    }

      // check if running in TypeScript runtime and path to load is TypeScript or Javascript file
    if (isRunningInTypeScriptRuntime() && !isTypeScriptFile(pathToLoad) && !isJavaScriptFile(pathToLoad)) {
      throw new Error(`Module ${pathToLoad} can not be loaded because running in TypeScript runtime and it is not a TypeScript or a JavaScript file.`);
    }

    // check if path to load is JavaScript file
    if (!isRunningInTypeScriptRuntime() && !isJavaScriptFile(pathToLoad)) {
      throw new Error(`Module ${pathToLoad} can not be loaded because it is not a JavaScript file.`);
    }

    // check if hook exists
    if (typeof hooks === 'object' && hooks !== null && typeof hooks.beforeLoad === 'function') {
      // call hook
      const result = await hooks.beforeLoad(pathToLoad);

      // explicitly check for false instead of falsy value
      if (result === false) {
        throw new ShouldNotBeLoadedError(pathToLoad);
      }
    }

    // load module
    const loadedModule: Module = {
      exports: await import(pathToLoad.toString()),
      path: pathToLoad,
      size: stats.size,
    };


    if (typeof hooks === 'object' && hooks !== null && typeof hooks.afterLoad === 'function') {
      await hooks.afterLoad(loadedModule);
    }

    return loadedModule;
  } catch (error) {
    if (typeof error === 'object' && error !== null && 'code' in error && error.code === 'ENOENT') {
      throw new Error(`'${pathToLoad}' can not be loaded because it does not exist.`);
    } else {
      throw error;
    }
  }
}

/**
 * Load all modules in a directory
 * @param directory Directory to load all modules from
 * @param recursive Whether or not to recursively load modules from sub directories
 * @param hooks Hooks for loading modules
 * @returns A promise that resolves with an array of modules
 */
export async function loadModules(
  directory: string,
  recursive = true,
  hooks?: Hooks,
): Promise<Module[]> {
  // read directory
  const files = await readdir(directory);

  // initialize flat array of modules
  const modules: Module[] = [];

  // iterate over all files in directory
  for (const file of files) {
    const pathToLoad = join(directory, file);
    const stats = await stat(pathToLoad);

    if (stats.isDirectory() && recursive) {
      modules.push(...await loadModules(pathToLoad, recursive, hooks));

      continue;
    }

    const moduleName = basename(pathToLoad, '.js');
    if (isRunningInTypeScriptRuntime() && isJavaScriptFile(pathToLoad) && existsSync(join(dirname(pathToLoad), `${moduleName}.ts`))) {
      console.info(`Skipping loading of module '${pathToLoad}' because running in TypeScript runtime and TypeScript module '${moduleName}.ts' exists in the same directory since it is assumed that the JavaScript file is the transpiled version of the TypeScript file.`);

      continue;
    }

    if (isRunningInTypeScriptRuntime() && isTypeScriptFile(pathToLoad) || isJavaScriptFile(pathToLoad)) {
      try {
        modules.push(await loadModule(pathToLoad, hooks));
      } catch (error) {
        if (error instanceof ShouldNotBeLoadedError) {
          // noop
        } else {
          throw error;
        }
      }
    }
  }

  return modules;
}
