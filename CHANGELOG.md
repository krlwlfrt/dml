## [3.0.3](https://gitlab.com/krlwlfrt/dml/compare/v3.0.2...v3.0.3) (2024-12-22)


### Bug Fixes

* pass hooks to recursive call ([a3745f6](https://gitlab.com/krlwlfrt/dml/commit/a3745f6df77886e38155bca1115607f785b34f7c))



## [3.0.2](https://gitlab.com/krlwlfrt/dml/compare/v3.0.1...v3.0.2) (2024-12-22)



## [3.0.1](https://gitlab.com/krlwlfrt/dml/compare/v3.0.0...v3.0.1) (2024-12-22)



# [3.0.0](https://gitlab.com/krlwlfrt/dml/compare/v2.0.1...v3.0.0) (2024-12-22)


### Features

* add hooks before and after loading modules ([837f373](https://gitlab.com/krlwlfrt/dml/commit/837f373a995920ce1ff16174ef6126e3329d6b5e))



## [2.0.1](https://gitlab.com/krlwlfrt/dml/compare/v2.0.0...v2.0.1) (2024-11-27)


### Bug Fixes

* make detection of Tsx more robust ([282ebfb](https://gitlab.com/krlwlfrt/dml/commit/282ebfb89417b4a809fbffdf72c33d19aafd29fb))



# [2.0.0](https://gitlab.com/krlwlfrt/dml/compare/v1.2.0...v2.0.0) (2024-11-27)


### Features

* add detection for Tsx and Bun ([0407dcc](https://gitlab.com/krlwlfrt/dml/commit/0407dcc1cac09e970569408bde905367caa22491))



# [1.2.0](https://gitlab.com/krlwlfrt/dml/compare/v1.1.0...v1.2.0) (2024-11-09)


### Features

* expand TypeScript runtime check to Deno ([b22fc7d](https://gitlab.com/krlwlfrt/dml/commit/b22fc7dbb838788a500c6891f52082faefd41145))



# [1.1.0](https://gitlab.com/krlwlfrt/dml/compare/v1.0.0...v1.1.0) (2024-11-09)



# [1.0.0](https://gitlab.com/krlwlfrt/dml/compare/v0.4.0...v1.0.0) (2023-02-28)



# [0.4.0](https://gitlab.com/krlwlfrt/dml/compare/v0.3.0...v0.4.0) (2022-09-07)



# [0.3.0](https://gitlab.com/krlwlfrt/dml/compare/v0.2.0...v0.3.0) (2021-07-29)



# [0.2.0](https://gitlab.com/krlwlfrt/dml/compare/v0.1.0...v0.2.0) (2021-03-05)



# [0.1.0](https://gitlab.com/krlwlfrt/dml/compare/v0.0.3...v0.1.0) (2021-02-23)


### Features

* return modules as objects with path and exports ([c5905cf](https://gitlab.com/krlwlfrt/dml/commit/c5905cf80c648619ac14cf98883dbd85d86231ba))



## [0.0.3](https://gitlab.com/krlwlfrt/dml/compare/v0.0.2...v0.0.3) (2021-02-23)



## [0.0.2](https://gitlab.com/krlwlfrt/dml/compare/v0.0.1...v0.0.2) (2021-02-23)



## [0.0.1](https://gitlab.com/krlwlfrt/dml/compare/74a2cf51b810eb5afcc3cb77c7711007986eddf6...v0.0.1) (2021-02-23)


### Features

* add implementation ([74a2cf5](https://gitlab.com/krlwlfrt/dml/commit/74a2cf51b810eb5afcc3cb77c7711007986eddf6))



