import {suite, test, timeout} from '@testdeck/mocha';
import {equal} from 'node:assert';
import {execSync} from 'node:child_process';

@suite(timeout(10000))
export class RuntimesSpec {
  @test
  async 'Bun'() {
    const result = execSync('bun test/runtimes.js');
    equal(result.toString(), `isRunningInBun true
isRunningInDeno false
isRunningInNodeJS true
isRunningInTSNode false
isRunningInTsx false
isRunningInTypeScriptRuntime true
`);
  }

  @test
  async 'Deno'() {
    const result = execSync('deno --allow-env --allow-read --unstable-sloppy-imports test/runtimes.js');
    equal(result.toString(), `isRunningInBun false
isRunningInDeno true
isRunningInNodeJS true
isRunningInTSNode false
isRunningInTsx false
isRunningInTypeScriptRuntime true
`);
  }

  @test
  async 'Node.js'() {
    // clear NODE_OPTIONS, so TS-Node is not loaded
    const result = execSync('NODE_OPTIONS="" node test/runtimes.js');

    equal(result.toString(), `isRunningInBun false
isRunningInDeno false
isRunningInNodeJS true
isRunningInTSNode false
isRunningInTsx false
isRunningInTypeScriptRuntime false
`);
  }

  @test
  async 'TS-Node'() {
    // clear NODE_OPTIONS, so TS-Node is not loaded
    const result = execSync('NODE_OPTIONS="" node --require ts-node/register test/runtimes.js');
    equal(result.toString(), `isRunningInBun false
isRunningInDeno false
isRunningInNodeJS true
isRunningInTSNode true
isRunningInTsx false
isRunningInTypeScriptRuntime true
`);
  }

  @test
  async 'Tsx'() {
    // clear NODE_OPTIONS, so TS-Node is not loaded
    const result = execSync('NODE_OPTIONS="" npx tsx test/runtimes.js');
    equal(result.toString(), `isRunningInBun false
isRunningInDeno false
isRunningInNodeJS true
isRunningInTSNode false
isRunningInTsx true
isRunningInTypeScriptRuntime true
`);
  }
}
