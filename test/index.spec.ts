import {suite, test} from '@testdeck/mocha';
import {join, resolve} from 'node:path';
import {isRunningInTSNode, loadModule, loadModules} from '../src';
import assert from 'node:assert';
import {registry} from './registry';
import {REGISTER_INSTANCE} from 'ts-node';

@suite
export class IndexSpec {
  static before() {
    // make sure that running in TS-Node
    assert(isRunningInTSNode(), `Is not running in TS.Node. Tests can only be run in TS-Node`);

    // clear registry
    registry.splice(0);
  }

  @test
  async 'load module'() {
    const moduleToLoad = resolve(__dirname, 'resources', 'dummy.ts');

    await loadModule(moduleToLoad);

    assert(registry.some((file) => file === moduleToLoad));
  }

  @test
  async 'recursively load modules'() {
    const pathToLoad = resolve(__dirname, 'resources', 'recursive');

    const modules = await loadModules(pathToLoad);

    const filesThatShouldHaveBeenLoaded = [
      join(pathToLoad, 'dummy.ts'),
      join(pathToLoad, 'deep', 'bar.js'),
      join(pathToLoad, 'deep', 'dummy.ts'),
      join(pathToLoad, 'deep', 'deeper', 'bar.ts'),
      join(pathToLoad, 'deep', 'deeper', 'dummy.ts'),
      join(pathToLoad, 'deep', 'deeper', 'foo.ts'),
    ];

    assert.equal(modules.length, 6, `Number of loaded modules is ${modules.length} instead of 6.`);

    for (const fileThatShouldHaveBeenLoaded of filesThatShouldHaveBeenLoaded) {
      assert(registry.some((file) => file === fileThatShouldHaveBeenLoaded), `${fileThatShouldHaveBeenLoaded} has not been loaded!`);
    }

    assert(registry.every((file) => file !== join(pathToLoad, 'deep', 'deeper', 'dummy.js')), `${join(pathToLoad, 'deep', 'deeper', 'dummy.js')} has been loaded!`);
  }

  @test
  async 'can not load modules that do not exist'() {
    return assert.rejects(loadModule(join(__dirname, 'dummy.ts')));
  }

  @test
  async 'can not load files that are not modules'() {
    return assert.rejects(loadModule(join(__dirname, 'resources', 'dummy.txt')));
  }

  @test
  async 'do not load TypeScript modules when not running in TS Node'() {
    // fake not running in TS-Node
    const service = process[REGISTER_INSTANCE];
    delete process[REGISTER_INSTANCE];

    const pathToLoad = resolve(__dirname, 'resources', 'recursive-not-in-ts-node');

    const modules = await loadModules(pathToLoad);

    const filesThatShouldHaveBeenLoaded = [
      join(pathToLoad, 'deep', 'bar.js'),
      join(pathToLoad, 'deep', 'deeper', 'dummy.js'),
    ];

    assert.equal(modules.length, 2, `Number of loaded modules is ${modules.length} instead of 2.`);

    for (const fileThatShouldHaveBeenLoaded of filesThatShouldHaveBeenLoaded) {
      assert(registry.some((file) => file === fileThatShouldHaveBeenLoaded), `${fileThatShouldHaveBeenLoaded} has not been loaded!`);
    }

    process[REGISTER_INSTANCE] = service;
  }

  @test
  async 'do not load files which are not modules'() {
    // fake not running in TS-Node
    const service = process[REGISTER_INSTANCE];
    delete process[REGISTER_INSTANCE];

    await assert.rejects(loadModule(join(__dirname, 'resources', 'dummy.txt')));

    process[REGISTER_INSTANCE] = service;
  }

  @test
  async 'do not load modules when before load hook returns false'() {
    const modules = await loadModules(join(__dirname, 'resources', 'hooks'), false, {
      afterLoad: async (module) => {
        console.info(`Loaded module ${module.path}.`);
      },
      beforeLoad: async (pathToLoad) => {
        return !pathToLoad.endsWith('bar.js');
      },
    });

    assert(modules.length === 1, `Number of loaded modules is ${modules.length} instead of 1.`);

    const fooModule = modules[0];
    assert(typeof fooModule.exports === 'object' && fooModule.exports !== null && 'foo' in fooModule.exports);
    assert(fooModule.exports.foo === 1);
  }

  @test
  async 'do not load directories as modules'() {
    assert.rejects(async () => {
      return loadModule(join(__dirname, 'foo.js'));
    });
  }
}
