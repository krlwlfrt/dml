const indexModule = require('../lib/index');

const functions = [
  indexModule.isRunningInBun,
  indexModule.isRunningInDeno,
  indexModule.isRunningInNodeJS,
  indexModule.isRunningInTSNode,
  indexModule.isRunningInTsx,
  indexModule.isRunningInTypeScriptRuntime,
];

for (const fn of functions) {
  console.log(fn.name, fn());
}
